package com.example.exception;

import com.example.customer.exception.model.CustomerAlreadyExistException;
import com.example.customer.exception.model.CustomerNotFoundException;
import com.example.exception.model.ErrorModel;
import com.example.file.exception.model.FileAlreadyExistException;
import com.example.file.exception.model.FileIsEmptyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = CustomerNotFoundException.class)
    public ResponseEntity<ErrorModel> getCustomerNotFoundException(CustomerNotFoundException ex){
        var errorModel = new ErrorModel(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        log.error(String.valueOf(errorModel));
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorModel);
    }

    @ExceptionHandler(value = CustomerAlreadyExistException.class)
    public ResponseEntity<ErrorModel> getCustomerAlreadyException(CustomerAlreadyExistException ex){
        var errorModel = new ErrorModel(HttpStatus.ALREADY_REPORTED.value(), ex.getMessage());
        log.error(String.valueOf(errorModel));
        return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(errorModel);
    }


    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
    public ResponseEntity<ErrorModel> getMaxUploadSizeExceededException(MaxUploadSizeExceededException ex){
        var errorModel = new ErrorModel(HttpStatus.REQUEST_ENTITY_TOO_LARGE.value(), ex.getMessage());
        log.error(String.valueOf(errorModel));
        return ResponseEntity.status(errorModel.getStatusCode()).body(errorModel);
    }

    @ExceptionHandler(value = FileIsEmptyException.class)
    public ResponseEntity<ErrorModel> getFileIsEmptyException(FileIsEmptyException ex){
        var errorModel = new ErrorModel(HttpStatus.CONFLICT.value(), ex.getMessage());
        log.error(String.valueOf(errorModel));
        return ResponseEntity.status(errorModel.getStatusCode()).body(errorModel);
    }

    @ExceptionHandler(value = FileAlreadyExistException.class)
    public ResponseEntity<ErrorModel> getFileIsEmptyException(FileAlreadyExistException ex){
        var errorModel = new ErrorModel(HttpStatus.CONFLICT.value(), ex.getMessage());
        log.error(String.valueOf(errorModel));
        return ResponseEntity.status(errorModel.getStatusCode()).body(errorModel);
    }
}
