package com.example.customer.service;

import com.example.customer.dto.CustomerDto;

import java.util.List;

public interface CustomerService {
    CustomerDto getById(Long id);
    void save(CustomerDto dto);
    List<CustomerDto> getAll();
}
