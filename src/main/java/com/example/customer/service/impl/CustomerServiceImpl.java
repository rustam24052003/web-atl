package com.example.customer.service.impl;

import com.example.customer.dto.CustomerDto;
import com.example.customer.exception.model.CustomerAlreadyExistException;
import com.example.customer.exception.model.CustomerNotFoundException;
import com.example.customer.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CustomerServiceImpl implements CustomerService {
    private static final List<CustomerDto> DTOS = new ArrayList<>();

    @Override
    public CustomerDto getById(Long id) {
        return DTOS.stream()
                .filter(dto -> dto.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new CustomerNotFoundException("CUSTOMER NOT FOUND WITH ID: " + id));
    }

    @Override
    public void save(CustomerDto dto) {
        var isPresent = DTOS.stream().anyMatch(data -> data.getId().equals(dto.getId()));
        if (isPresent)
            throw new CustomerAlreadyExistException("CUSTOMER ALREADY EXIST WITH ID: " + dto.getId());
        DTOS.add(dto);
    }

    @Override
    public List<CustomerDto> getAll() {
        return DTOS;
    }
}
