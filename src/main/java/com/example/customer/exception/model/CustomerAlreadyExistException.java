package com.example.customer.exception.model;

public class CustomerAlreadyExistException extends RuntimeException{
    private String message;

    public CustomerAlreadyExistException() {}

    public CustomerAlreadyExistException(String message) {
        super(message);
        this.message = message;
    }
}
