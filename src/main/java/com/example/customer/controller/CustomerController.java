package com.example.customer.controller;

import com.example.customer.dto.CustomerDto;
import com.example.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService CUSTOMER_SERVICE;

    @GetMapping("/id/{id}")
    public CustomerDto getById(@PathVariable Long id) {
        return CUSTOMER_SERVICE.getById(id);
    }

    @PostMapping("/save")
    public void save(@RequestBody CustomerDto dto) {
        CUSTOMER_SERVICE.save(dto);
    }

    @GetMapping("/all")
    public List<CustomerDto> getAll() {
        return CUSTOMER_SERVICE.getAll();
    }
}
