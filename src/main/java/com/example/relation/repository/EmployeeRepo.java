package com.example.relation.repository;

import com.example.relation.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface EmployeeRepo extends CrudRepository<EmployeeEntity, Long> {

    @Modifying
    @Transactional
    @Query("update EmployeeEntity2 e set e.salary = :newSalary where e.id = :employeeId")
    void updateSalaryById(@Param("employeeId") Long employeeId, @Param("newSalary") float newSalary);
}