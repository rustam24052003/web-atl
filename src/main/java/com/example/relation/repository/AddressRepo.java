package com.example.relation.repository;

import com.example.relation.entity.AddressEntity;
import com.example.relation.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface AddressRepo extends CrudRepository<AddressEntity, Long> {
    boolean existsByEmployeeEntity(EmployeeEntity employeeEntity);

    Optional<AddressEntity> findByEmployeeEntity_Id(Long employeeID);

    @Query("select a from AddressEntity a join fetch a.employeeEntity e where e.name = :employeeName")
    Optional<AddressEntity> getAddressEntitiesByEmployeeName(@Param("employeeName") String employeeName);

    @Modifying
    @Transactional
    @Query("update AddressEntity a set a.country = :newCountry, a.city = :newCity where a.employeeEntity.id = :employeeId")
    void updateAddressByEmployeeId(@Param("employeeId") Long employeeId,
                                   @Param("newCountry") String newCountry,
                                   @Param("newCity") String newCity);

}
