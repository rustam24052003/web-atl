package com.example.relation.repository;

import com.example.relation.entity.DepartmentEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepo extends CrudRepository<DepartmentEntity, Long> {

    Optional<DepartmentEntity> getByName(String name);

    @Query("SELECT d FROM DepartmentEntity d JOIN FETCH d.employeeEntities e WHERE d.name = :departmentName")
    Optional<DepartmentEntity> findByNameAndFetchEmployees(@Param("departmentName") String departmentName);

    @Query(value = "SELECT e.name AS employeeName, e.salary AS employeeSalary " +
            "FROM relation.department d " +
            "JOIN relation.employee e ON d.depertment_id = e.depertment_id " +
            "WHERE d.name = :departmentName",
            nativeQuery = true)
    List<Object[]> findEmployeeNameAndSalaryByDepartmentName(@Param("departmentName") String departmentName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE relation.employee e " +
            "SET salary = e.salary * (1 + :percentage)" +
            "WHERE e.depertment_id = :departmentId",
            nativeQuery = true)
    void increaseSalariesByDepartmentId(@Param("departmentId") Long departmentId,
                                       @Param("percentage") double percentage);


    @Query(value = "select *from relation.department where name like :nameLetter%", nativeQuery = true)
    Optional<List<DepartmentEntity>> getDepartmentEntityByNameLetter(@Param("nameLetter") String nameLetter);
}
