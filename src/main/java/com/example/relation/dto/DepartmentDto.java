package com.example.relation.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentDto {
    String name;

    LocalDateTime createdAt;

    LocalDateTime updateAt;

    List<EmployeeDto> employeeDtos;
}
