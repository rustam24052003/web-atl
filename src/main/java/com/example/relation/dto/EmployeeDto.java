package com.example.relation.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmployeeDto {

    @Size(min = 5, max = 15, message = "position name must be between 10 and 200 characters")
    String position;

    @NotNull(message = "name can't be null")
    String name;

    @NotEmpty(message = "Second name must not be null, empty value/space can be considered")
    String surname;

    @Positive(message = "salary can't be negative")
    float salary;

    @NotNull
    String dob;

    @Min(value = 18, message = "Age must be greater than 18")
    @Max(value = 25, message = "Age must be smaller than 25")
    int age;
    LocalDateTime createdAt;

    LocalDateTime updateAt;
}
