package com.example.relation.dto;

import com.example.relation.entity.EmployeeEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressDto {

    String country;

    String city;

    EmployeeDto employeeDto;
}
