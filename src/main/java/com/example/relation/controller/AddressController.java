package com.example.relation.controller;

import com.example.relation.dto.AddressDto;
import com.example.relation.model.MessageModel;
import com.example.relation.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping("/save/{employeeId}")
    public ResponseEntity<MessageModel<AddressDto>> save(@PathVariable Long employeeId, @RequestBody AddressDto addressDto) {
        return addressService.save(employeeId, addressDto);
    }

    @GetMapping("/name/{employeeName}")
    public ResponseEntity<MessageModel<AddressDto>> getAddressEntitiesByEmployeeName(@PathVariable String employeeName) {
        return addressService.getAddressEntitiesByEmployeeName(employeeName);
    }

    @PutMapping("/updt/{employeeId}/{newCountry}/{newCity}")
    public ResponseEntity<MessageModel<AddressDto>> updateAddressByEmployeeId(@PathVariable Long employeeId,
                                                                              @PathVariable String newCountry,
                                                                              @PathVariable String newCity) {
        return addressService.updateAddressByEmployeeId(employeeId, newCountry, newCity);
    }
}
