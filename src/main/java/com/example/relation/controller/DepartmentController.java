package com.example.relation.controller;

import com.example.relation.dto.DepartmentDto;
import com.example.relation.dto.EmployeeDto;
import com.example.relation.model.MessageModel;
import com.example.relation.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/department")
@RequiredArgsConstructor
public class DepartmentController {

    private final DepartmentService departmentService;


    @PostMapping("save")
    public ResponseEntity<MessageModel<DepartmentDto>> save(@RequestBody DepartmentDto dto) {
        return departmentService.save(dto);
    }

    @GetMapping("/id/{id}")
    public DepartmentDto getById(@PathVariable Long id) {
        return departmentService.getById(id);
    }


    @GetMapping("/emp/{departmentName}")
    public ResponseEntity<MessageModel<List<EmployeeDto>>> getAllEmployeeByDepartmentName(@PathVariable String departmentName) {
        return departmentService.getAllEmployeeByDepartmentName(departmentName);
    }

    @GetMapping("/name/{departmentName}")
    public List<Object[]> findEmployeeNameAndSalaryByDepartmentName(@PathVariable String departmentName) {
        return departmentService.findEmployeeNameAndSalaryByDepartmentName(departmentName);
    }

    @PutMapping("/updt/{departmentId}/{percentage}")
    public ResponseEntity<MessageModel<DepartmentDto>> increaseSalariesByDepartmentId(@PathVariable Long departmentId,
                                                                                      @PathVariable double percentage) {
        return departmentService.increaseSalariesByDepartmentId(departmentId, percentage);
    }

    @GetMapping("/letter/{nameLetter}")
    public ResponseEntity<MessageModel<List<DepartmentDto>>> getDepartmentEntityByNameLetter(@PathVariable String nameLetter) {
        return departmentService.getDepartmentEntityByNameLetter(nameLetter);
    }
}
