package com.example.relation.controller;

import com.example.relation.dto.EmployeeDto;
import com.example.relation.model.MessageModel;
import com.example.relation.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
@Validated
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/save/{depId}")
    public ResponseEntity<MessageModel<EmployeeDto>> save(@PathVariable Long depId, @RequestBody @Valid EmployeeDto dto) {
        return employeeService.save(depId, dto);
    }

    @PutMapping("/updt/{employeeId}/{newSalary}")
    public ResponseEntity<MessageModel<EmployeeDto>> updateSalaryById(@PathVariable Long employeeId, @PathVariable float newSalary) {
        return employeeService.updateSalaryById(employeeId, newSalary);
    }
}