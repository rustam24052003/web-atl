package com.example.relation.mapping;

import com.example.relation.dto.DepartmentDto;
import com.example.relation.entity.DepartmentEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DepartmentMapper {

    private final EmployeeMapper employeeMapper;

    public DepartmentEntity MapDtoToEntity(DepartmentDto dto){
        return DepartmentEntity.builder()
                .name(dto.getName())
                .createdAt(dto.getCreatedAt())
                .updateAt(dto.getUpdateAt())
                .build();
    }

    public DepartmentDto MapEntityToDto(DepartmentEntity entity){
        return DepartmentDto.builder()
                .name(entity.getName())
                .createdAt(entity.getCreatedAt())
                .updateAt(entity.getUpdateAt())
                .employeeDtos(employeeMapper.MapToDtoList(entity.getEmployeeEntities()))
                .build();
    }

    public List<DepartmentDto> MapToDtoList(List<DepartmentEntity> entities){
        return entities.stream()
                .map(this::MapEntityToDto)
                .collect(Collectors.toList());
    }
}
