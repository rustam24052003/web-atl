package com.example.relation.mapping;

import com.example.relation.dto.AddressDto;
import com.example.relation.entity.AddressEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AddressMapper {

    private final EmployeeMapper employeeMapper;

    public AddressEntity MapDtoToEntity(AddressDto dto){
        return AddressEntity.builder()
                .country(dto.getCountry())
                .city(dto.getCity())
                .build();
    }

    public AddressDto MapEntityToDto(AddressEntity entity){
        return AddressDto.builder()
                .country(entity.getCountry())
                .city(entity.getCity())
                .employeeDto(employeeMapper.MapEntityToDto(entity.getEmployeeEntity()))
                .build();
    }

    public List<AddressDto> MapToDtoList(List<AddressEntity> entities){
        return entities.stream()
                .map(this::MapEntityToDto)
                .collect(Collectors.toList());
    }
}
