package com.example.relation.mapping;

import com.example.relation.dto.EmployeeDto;
import com.example.relation.entity.EmployeeEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class EmployeeMapper {

    public EmployeeEntity MapDtoToEntity(EmployeeDto dto){
        return EmployeeEntity.builder()
                .position(dto.getPosition())
                .name(dto.getName())
                .surname(dto.getSurname())
                .salary(dto.getSalary())
                .createdAt(dto.getCreatedAt())
                .updateAt(dto.getUpdateAt())
                .build();
    }

    public EmployeeDto MapEntityToDto(EmployeeEntity entity){
        return EmployeeDto.builder()
                .position(entity.getPosition())
                .name(entity.getName())
                .surname(entity.getSurname())
                .salary(entity.getSalary())
                .createdAt(entity.getCreatedAt())
                .updateAt(entity.getUpdateAt())
                .build();
    }

    public List<EmployeeDto> MapToDtoList(List<EmployeeEntity> entities){
        return entities.stream()
                .map(this::MapEntityToDto)
                .collect(Collectors.toList());
    }
}