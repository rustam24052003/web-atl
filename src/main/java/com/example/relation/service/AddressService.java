package com.example.relation.service;

import com.example.relation.dto.AddressDto;
import com.example.relation.model.MessageModel;
import org.springframework.http.ResponseEntity;

public interface AddressService {
    ResponseEntity<MessageModel<AddressDto>> save(Long employeeId, AddressDto addressDto);
    ResponseEntity<MessageModel<AddressDto>> getAddressEntitiesByEmployeeName(String employeeName);
    ResponseEntity<MessageModel<AddressDto>> updateAddressByEmployeeId(Long employeeId,String newCountry,String newCity);
}
