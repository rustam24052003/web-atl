package com.example.relation.service;

import com.example.relation.dto.EmployeeDto;
import com.example.relation.model.MessageModel;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;

public interface EmployeeService {
    ResponseEntity<MessageModel<EmployeeDto>> save(Long departmentId,EmployeeDto dto);
    ResponseEntity<MessageModel<EmployeeDto>> updateSalaryById(Long employeeId, float newSalary);
}
