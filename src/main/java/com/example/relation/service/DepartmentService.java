package com.example.relation.service;

import com.example.relation.dto.DepartmentDto;
import com.example.relation.dto.EmployeeDto;
import com.example.relation.model.MessageModel;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface DepartmentService {

    ResponseEntity<MessageModel<DepartmentDto>> save(DepartmentDto departmentDto);

    DepartmentDto getById(Long id);

    ResponseEntity<MessageModel<List<EmployeeDto>>> getAllEmployeeByDepartmentName(String departmentName);

    List<Object[]> findEmployeeNameAndSalaryByDepartmentName(String departmentName);

    ResponseEntity<MessageModel<DepartmentDto>> increaseSalariesByDepartmentId(Long departmentId, double percentage);

    ResponseEntity<MessageModel<List<DepartmentDto>>> getDepartmentEntityByNameLetter(String nameLetter);
}