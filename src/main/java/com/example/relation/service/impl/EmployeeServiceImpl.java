package com.example.relation.service.impl;

import com.example.relation.dto.EmployeeDto;
import com.example.relation.entity.DepartmentEntity;
import com.example.relation.entity.EmployeeEntity;
import com.example.relation.mapping.EmployeeMapper;
import com.example.relation.model.MessageModel;
import com.example.relation.repository.DepartmentRepo;
import com.example.relation.repository.EmployeeRepo;
import com.example.relation.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepo employeeRepo;
    private final EmployeeMapper mapper;

    private final DepartmentRepo departmentRepo;

    @Override
    public ResponseEntity<MessageModel<EmployeeDto>> save(Long departmentId, EmployeeDto dto) {
        DepartmentEntity department = departmentRepo.findById(departmentId).orElseThrow();
        EmployeeEntity employee = mapper.MapDtoToEntity(dto);


        department.getEmployeeEntities().add(employee);

        employeeRepo.save(employee);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new MessageModel<>("Lesson saved successfully", dto));
    }

    @Override
    public ResponseEntity<MessageModel<EmployeeDto>> updateSalaryById(Long employeeId, float newSalary) {
        employeeRepo.updateSalaryById(employeeId, newSalary);
        return ResponseEntity.status(HttpStatus.UPGRADE_REQUIRED)
                .body(new MessageModel<>("new salary", mapper.MapEntityToDto(employeeRepo.findById(employeeId).orElseThrow())));
    }
}
