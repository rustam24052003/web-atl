package com.example.relation.service.impl;

import com.example.relation.dto.DepartmentDto;
import com.example.relation.dto.EmployeeDto;
import com.example.relation.entity.DepartmentEntity;
import com.example.relation.mapping.DepartmentMapper;
import com.example.relation.mapping.EmployeeMapper;
import com.example.relation.model.MessageModel;
import com.example.relation.repository.DepartmentRepo;
import com.example.relation.repository.EmployeeRepo;
import com.example.relation.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepo departmentRepo;
    private final DepartmentMapper mapper;

    private final EmployeeRepo employeeRepo;
    private final EmployeeMapper employeeMapper;

    @Override
    public ResponseEntity<MessageModel<DepartmentDto>> save(DepartmentDto dto) {
        DepartmentEntity entity = mapper.MapDtoToEntity(dto);

        departmentRepo.save(entity);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new MessageModel<>("Lesson saved successfully", dto));
    }

    @Override
    public DepartmentDto getById(Long id) {

        return mapper.MapEntityToDto(departmentRepo.findById(id).orElseThrow());
    }


    public ResponseEntity<MessageModel<List<EmployeeDto>>> getAllEmployeeByDepartmentName(String departmentName) {
        DepartmentEntity department = departmentRepo.findByNameAndFetchEmployees(departmentName).orElseThrow();
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(new MessageModel<>("all employees",
                        employeeMapper.MapToDtoList(department.getEmployeeEntities())));
    }

    @Override
    public List<Object[]> findEmployeeNameAndSalaryByDepartmentName(String departmentName) {

        return departmentRepo.findEmployeeNameAndSalaryByDepartmentName(departmentName);
    }

    @Override
    public ResponseEntity<MessageModel<DepartmentDto>> increaseSalariesByDepartmentId(Long departmentId, double percentage) {
        departmentRepo.increaseSalariesByDepartmentId(departmentId, percentage);
        return ResponseEntity.status(HttpStatus.UPGRADE_REQUIRED)
                .body(new MessageModel<>("new employee salaries for department with ID: " + departmentId,
                        mapper.MapEntityToDto(departmentRepo.findById(departmentId).orElseThrow())));
    }

    @Override
    public ResponseEntity<MessageModel<List<DepartmentDto>>> getDepartmentEntityByNameLetter(String nameLetter) {
        List<DepartmentDto> departmentDto = mapper.MapToDtoList(departmentRepo.getDepartmentEntityByNameLetter(nameLetter).orElseThrow());
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(new MessageModel<>("department by name",departmentDto));
    }
}
