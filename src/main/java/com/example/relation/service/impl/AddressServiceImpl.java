package com.example.relation.service.impl;

import com.example.relation.dto.AddressDto;
import com.example.relation.entity.AddressEntity;
import com.example.relation.entity.EmployeeEntity;
import com.example.relation.mapping.AddressMapper;
import com.example.relation.model.MessageModel;
import com.example.relation.repository.AddressRepo;
import com.example.relation.repository.EmployeeRepo;
import com.example.relation.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepo addressRepo;
    private final AddressMapper addressMapper;

    private final EmployeeRepo employeeRepo;

    @Override
    public ResponseEntity<MessageModel<AddressDto>> save(Long employeeId, AddressDto addressDto) {
        EmployeeEntity employeeEntity = employeeRepo.findById(employeeId)
                .orElseThrow(() -> new EntityNotFoundException("Employee with ID " + employeeId + " not found"));

        boolean employeeHasAddress = addressRepo.existsByEmployeeEntity(employeeEntity);
        if (employeeHasAddress) {
            throw new IllegalArgumentException("Each employee can have at most one address");
        }

        AddressEntity addressEntity = addressMapper.MapDtoToEntity(addressDto);

        addressEntity.setEmployeeEntity(employeeEntity);
        addressRepo.save(addressEntity);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new MessageModel<>("Address created successfully", addressDto));
    }

    @Override
    public ResponseEntity<MessageModel<AddressDto>> getAddressEntitiesByEmployeeName(String employeeName) {
        AddressEntity addressEntity = addressRepo.getAddressEntitiesByEmployeeName(employeeName).orElseThrow();

        return ResponseEntity.status(HttpStatus.FOUND)
                .body(new MessageModel<>("address by employee name", addressMapper.MapEntityToDto(addressEntity)));
    }

    @Override
    public ResponseEntity<MessageModel<AddressDto>> updateAddressByEmployeeId(Long employeeId, String newCountry, String newCity) {
        addressRepo.updateAddressByEmployeeId(employeeId, newCountry, newCity);
        return ResponseEntity.status(HttpStatus.UPGRADE_REQUIRED)
                .body(new MessageModel<>("new address",
                        addressMapper.MapEntityToDto(addressRepo.findByEmployeeEntity_Id(employeeId).orElseThrow())));
    }
}

