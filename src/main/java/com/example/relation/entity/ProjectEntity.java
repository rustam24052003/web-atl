package com.example.relation.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "project", schema = "relation")
public class ProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    Long id;

    @Column(name = "start_date", nullable = false)
    LocalDateTime startAt;

    @Column(name = "expired_date", nullable = false)
    LocalDateTime expiredAt;

    @Column(name = "members", nullable = false)
    int members;

    @ManyToMany(targetEntity = EmployeeEntity.class,
            cascade = {CascadeType.MERGE, CascadeType.MERGE})
    @JoinTable(name = "project_employee", schema = "relation",
    joinColumns = @JoinColumn(name = "project_id"),
    inverseJoinColumns = @JoinColumn(name = "employee_id"))
    List<ProjectEntity> projectEntities;
}
