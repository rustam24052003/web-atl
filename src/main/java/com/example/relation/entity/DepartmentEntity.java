package com.example.relation.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "department", schema = "relation")
public class DepartmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "depertment_id")
    Long id;

    @Column(name = "name", length = 20, nullable = false)
    String name;

    @CreationTimestamp
    @Column(name = "created_at")
    LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "update_at")
    LocalDateTime updateAt;


    @OneToMany(targetEntity = EmployeeEntity.class,
            cascade = {CascadeType.MERGE, CascadeType.MERGE}, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "depertment_id", name = "depertment_id")
    List<EmployeeEntity> employeeEntities;
}
