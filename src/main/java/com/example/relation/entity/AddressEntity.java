package com.example.relation.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "address", schema = "relation")
public class AddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    Long id;

    @Column(name = "country", length = 30, nullable = false)
    String country;

    @Column(name = "city", length = 30, nullable = false)
    String city;

    @OneToOne(targetEntity = EmployeeEntity.class,
            cascade = {CascadeType.MERGE, CascadeType.MERGE}, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "employee_id", name = "employee_id")
    EmployeeEntity employeeEntity;
}
