package com.example.library.controller;

import com.example.library.dto.BookDto;
import com.example.library.entity.BookEntity;
import com.example.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/library/book")
@RequiredArgsConstructor
public class BookController {

    @Qualifier("bookServiceImpl")
    private final BookService bookService;


    @PostMapping("/save")
    public ResponseEntity<BookDto> save(@RequestBody BookDto bookDto) {
        return bookService.save(bookDto);
    }


    @GetMapping("/all")
    public ResponseEntity<List<BookDto>> getAll() {
        return bookService.getAll();
    }


    @GetMapping("/id/{id}")
    public ResponseEntity<BookDto> getById(@PathVariable Long id) {
        return bookService.getById(id);
    }


    @DeleteMapping("/id/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        return bookService.deleteById(id);
    }


    @PutMapping("/id/{id}")
    public ResponseEntity<Void> update(@PathVariable Long id, @RequestBody BookEntity bookEntity) {
        return bookService.update(id, bookEntity);
    }
}
