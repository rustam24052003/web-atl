package com.example.library.model;

import java.time.LocalDateTime;

public class Book {
    private Long id;

    private String title;

    private String description;

    private boolean isCompleted;

    private LocalDateTime createdDateTime;
}
