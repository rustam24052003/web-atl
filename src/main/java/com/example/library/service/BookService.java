package com.example.library.service;

import com.example.library.dto.BookDto;
import com.example.library.entity.BookEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BookService {
    ResponseEntity<BookDto> save(BookDto bookDto);
    ResponseEntity<List<BookDto>> getAll();
    ResponseEntity<BookDto> getById(Long id);
    ResponseEntity<Void> deleteById(Long id);
    ResponseEntity<Void> update(Long id, BookEntity bookEntity);
}
