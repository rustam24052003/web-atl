package com.example.library.service.impl;

import com.example.library.dto.BookDto;
import com.example.library.entity.BookEntity;
import com.example.library.repository.BookRepository;
import com.example.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("bookServiceImpl")
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepo;

    @Override
    public ResponseEntity<BookDto> save(BookDto bookDto) {
        BookEntity bookEntity = MapDtoToEntity(bookDto);
        bookRepo.save(bookEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookDto);
    }

    @Override
    public ResponseEntity<List<BookDto>> getAll() {
        List<BookDto> dtoList = MapToDtoList((List<BookEntity>) bookRepo.findAll());
        return ResponseEntity.status(HttpStatus.FOUND).body(dtoList);
    }

    @Override
    public ResponseEntity<BookDto> getById(Long id) {
        return bookRepo.findById(id)
                .map(bookEntity -> ResponseEntity.status(HttpStatus.FOUND)
                        .body(MapEntityToDto(bookEntity)))
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @Override
    public ResponseEntity<Void> deleteById(Long id) {
        if (!bookRepo.existsById(id))
            return ResponseEntity.notFound().build();
        bookRepo.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> update(Long id, BookEntity bookEntity) {
        Optional<BookEntity> bookOptional = bookRepo.findById(id);
        if (bookOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        BookEntity book = bookOptional.get();
        book.setTitle(bookEntity.getTitle());
        book.setDescription(bookEntity.getDescription());
        book.setCompleted(bookEntity.isCompleted());
        bookRepo.save(book);
        return ResponseEntity.ok().build();
    }


    private BookEntity MapDtoToEntity(BookDto bookDto){
        return BookEntity.builder()
                .title(bookDto.getTitle())
                .description(bookDto.getDescription())
                .completed(bookDto.isCompleted())
                .build();
    }

    private List<BookDto> MapToDtoList(List<BookEntity> entities){
        return entities.stream()
                .map(bookEntity ->
                        BookDto.builder()
                        .title(bookEntity.getTitle())
                        .description(bookEntity.getDescription())
                        .isCompleted(bookEntity.isCompleted())
                        .build())
                .collect(Collectors.toList());
    }

    private BookDto MapEntityToDto(BookEntity bookEntity){
        return BookDto.builder()
                .title(bookEntity.getTitle())
                .description(bookEntity.getDescription())
                .isCompleted(bookEntity.isCompleted())
                .build();
    }
}
