package com.example.library.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book", schema = "library")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookEntity {
    @Id
    @Column(name = "book_id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "title")
    String title;

    @Column(name = "book_description")
    String description;

    @Column(name = "isCompleted")
    private boolean completed;

    @CreationTimestamp
    @Column(name = "created_at")
    LocalDateTime createdDateTime;

    @UpdateTimestamp
    @Column(name = "update_at")
    LocalDateTime updateDateTime;
}
