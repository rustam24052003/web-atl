package com.example.thymeleaf.repository;

import com.example.thymeleaf.entity.EmployeeEntity2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo2 extends JpaRepository<EmployeeEntity2, Long> {
}
