package com.example.thymeleaf.service.impl;

import com.example.thymeleaf.entity.EmployeeEntity2;
import com.example.thymeleaf.repository.EmployeeRepo2;
import com.example.thymeleaf.service.EmployeeService2;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class EmployeeService2Impl implements EmployeeService2 {

    EmployeeRepo2 employeeRepo2;

    @Override
    public void save(EmployeeEntity2 employeeEntity2) {
        employeeRepo2.save(employeeEntity2);
    }

    @Override
    public List<EmployeeEntity2> getAllEmployee() {
        return employeeRepo2.findAll();
    }

    @Override
    public EmployeeEntity2 getById(Long id) {
        return employeeRepo2.findById(id).orElseThrow();
    }

    @Override
    public void deleteAll() {
        employeeRepo2.deleteAll();
    }

    @Override
    public void deleteById(Long id) {
        employeeRepo2.deleteById(id);
    }

    @Override
    public void update(Long id, EmployeeEntity2 employeeEntity2) {
        EmployeeEntity2 pendir = employeeRepo2.findById(id).orElseThrow();

        pendir.setName(pendir.getName());
        pendir.setSalary(employeeEntity2.getSalary());
        pendir.setDob(employeeEntity2.getDob());
        pendir.setEmail(employeeEntity2.getEmail());
        pendir.setAdmin(employeeEntity2.isAdmin());

        employeeRepo2.save(pendir);
    }
}
