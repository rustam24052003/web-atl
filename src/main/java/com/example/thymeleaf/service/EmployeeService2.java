package com.example.thymeleaf.service;

import com.example.thymeleaf.entity.EmployeeEntity2;

import java.util.List;

public interface EmployeeService2 {
    void save(EmployeeEntity2 employeeEntity2);
    List<EmployeeEntity2> getAllEmployee();
    EmployeeEntity2 getById(Long id);
    void deleteAll();
    void deleteById(Long id);
    void update(Long id, EmployeeEntity2 employeeEntity2);
}
