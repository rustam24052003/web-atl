package com.example.thymeleaf.controller;

import com.example.thymeleaf.entity.EmployeeEntity2;
import com.example.thymeleaf.service.EmployeeService2;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/v2/api/employee")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class EmployeeController2 {
    EmployeeService2 employeeService2;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("allEmployees", employeeService2.getAllEmployee());
        return "intro";
    }

    @GetMapping("/addNew")
    public String addNewEmployee(Model model) {
        EmployeeEntity2 employeeEntity2 = new EmployeeEntity2();
        model.addAttribute("employee", employeeEntity2);
        return "save";
    }
    @PostMapping("/save")
    public String saveEmployee(@ModelAttribute("employee")EmployeeEntity2 employeeEntity2) {
        employeeService2.save(employeeEntity2);
        return "redirect:/v2/api/employee/";
    }
}
