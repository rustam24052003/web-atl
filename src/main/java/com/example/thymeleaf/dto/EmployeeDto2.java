package com.example.thymeleaf.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmployeeDto2 {

    Long id;

    String name;

    BigDecimal salary;

    LocalDate dob;

    String email;

    boolean isAdmin;
}
