package com.example.thymeleaf.mapping;

import com.example.thymeleaf.dto.EmployeeDto2;
import com.example.thymeleaf.entity.EmployeeEntity2;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface EmployeeMapper2 {
    EmployeeEntity2 mapDtoToEntity(EmployeeDto2 employeeDto2);

    EmployeeDto2 mapEntityToDto(EmployeeEntity2 employeeEntity2);

    List<EmployeeDto2> mapToDtoList(List<EmployeeEntity2> pendirEntities);
}
