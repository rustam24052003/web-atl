package com.example.thymeleaf.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "employee", schema = "thymeleaf")
public class EmployeeEntity2 {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pendir_id")
    Long id;

    @NotBlank(message = "Name is required")
    @Size(max = 20, message = "Name must be 20 characters or fewer")
    @Column(name = "employee_name", nullable = false)
    String name;

    @NotNull(message = "Salary is required")
    @DecimalMin(value = "0.0", message = "Salary must be a positive value")
    @Column(name = "salary", nullable = false)
    BigDecimal salary;

    @NotNull(message = "Date of birth is required")
    @Column(name = "employee_dob", nullable = false)
    LocalDate dob;

    @NotBlank(message = "Email is required")
    @Email(message = "Email must be valid")
    @Pattern(regexp = ".*@.*", message = "Email must contain an '@' symbol")
    @Column(name = "email", nullable = false)
    String email;

    @Column(name = "is_admin", nullable = false)
    boolean isAdmin;
}
