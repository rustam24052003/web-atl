package com.example;

import com.example.internationalization.config.LocalizationModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class WebAtlApplication implements CommandLineRunner {

	private final LocalizationModel localizationModel;
	private final MessageSource messageSource;

	public static void main(String[] args) {
		SpringApplication.run(WebAtlApplication.class, args);
	}

	@Override
	public void run(String... args) {
		String[] strings = {"Ali", "Mardan"};
		log.info(messageSource.getMessage("greetings", strings, localizationModel.getDefaultLocale()));
	}

	@Bean
	public AcceptHeaderLocaleResolver localeResolver() {
		AcceptHeaderLocaleResolver resolver = new AcceptHeaderLocaleResolver();
		resolver.setDefaultLocale(localizationModel.getDefaultLocale());
		resolver.setSupportedLocales(localizationModel.getSupportedLocales());
		return resolver;
	}

	@Bean
	public LocaleChangeInterceptor localeInterceptor() {
		LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
		localeInterceptor.setParamName("Accept-Language");
		return localeInterceptor;
	}
}
