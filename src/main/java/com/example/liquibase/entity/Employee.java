package com.example.liquibase.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;

    private String position;
    private String name;
    private String surname;
    private BigDecimal salary;
    private Timestamp createdAt;

    // Getters and Setters
}
