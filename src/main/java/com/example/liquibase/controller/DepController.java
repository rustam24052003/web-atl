package com.example.liquibase.controller;

import com.example.liquibase.entity.Department;
import com.example.liquibase.repository.DepRepo;
import com.example.relation.entity.DepartmentEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/liquibase")
public class DepController {
    private final DepRepo departmentRepo;

    @GetMapping("/all")
    public List<Department> getAllEmployees() {
        return departmentRepo.findAll();
    }

    @PostMapping ("/save")
    public void save(DepartmentEntity departmentEntity){

    }
}
