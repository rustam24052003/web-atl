package com.example.liquibase.controller;

import com.example.liquibase.entity.Employee;
import com.example.liquibase.repository.EmpRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/liquibase")
public class EmpController {
    private final EmpRepo employeeRepo;

    @GetMapping("/allEmp")
    public List<Employee> getAllEmployees() {
        return employeeRepo.findAll();
    }
}
