package com.example.file.service.impl;

import com.example.file.exception.model.FileAlreadyExistException;
import com.example.file.exception.model.FileIsEmptyException;
import com.example.file.exception.model.InvalidDirectoryOrNotExistException;
import com.example.file.model.FileRequest;
import com.example.file.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class FileServiceImpl implements FileService {
    @Override
    public ResponseEntity<String> upload(FileRequest fileRequest) {
        MultipartFile file = fileRequest.getFile();
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        log.info(file.getContentType());
        Path destination = Paths.get("rootDir")
                .resolve(Objects.requireNonNull(file.getOriginalFilename()))
                .normalize().toAbsolutePath();

        try {
            if (file.isEmpty()) {
                throw new FileIsEmptyException("You can't upload empty file!");
            }
            Files.copy(file.getInputStream(), destination);
        } catch (IOException e) {
            throw new FileAlreadyExistException("You can't download two files with the same name!");
        }
        log.info(file.getContentType());
        return new ResponseEntity<>(destination.toString(), HttpStatus.OK);
    }

    @Override
    public List<String> getFiles() {
        File directory = new File("rootDir");

        if (!directory.exists() || !directory.isDirectory()) {
            throw new InvalidDirectoryOrNotExistException("Invalid directory path or directory does not exist.");
        }

        List<String> fileNames = new ArrayList<>();
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (file.isFile()) {
                fileNames.add(file.getName());
            }
        }

        if (fileNames.isEmpty()) {
            return Collections.singletonList("Directory is empty.");
        }

        return fileNames;
    }

    @Override
    public ResponseEntity<InputStreamResource> download(String pathName) throws IOException {
        Path destination = Paths.get("rootDir/" + pathName);
        File file = new File(destination.toUri());
        if (!file.exists()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        String fileName = file.getName();
        FileInputStream inputStream = new FileInputStream(file);
        String contentType = Files.probeContentType(destination);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(contentType));
        headers.setContentDispositionFormData(fileName, fileName);
        headers.setContentLength(file.length());

        InputStreamResource resource = new InputStreamResource(inputStream);

        return ResponseEntity.ok()
                .headers(headers)
                .body(resource);
    }
}
