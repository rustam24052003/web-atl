package com.example.file.service;

import com.example.file.model.FileRequest;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

public interface FileService {
    ResponseEntity<String> upload(FileRequest fileRequest);
    List<String> getFiles();
    ResponseEntity<InputStreamResource> download(String pathName) throws IOException;
}
