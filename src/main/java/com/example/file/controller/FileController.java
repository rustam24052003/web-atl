package com.example.file.controller;

import com.example.file.model.FileRequest;
import com.example.file.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.*;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {

    private final FileService fileService;

    @GetMapping("/info")
    private void getFileInfo(@RequestPart("file") MultipartFile file){
        log.info(file.getName());
        log.info(file.getOriginalFilename());
        log.info(file.getContentType());
    }

    @GetMapping("/info2")
    private void getFileInfo2(FileRequest file){
        log.info(file.getFileName());
        log.info(file.getFolderName());
        log.info(file.getFile().getContentType());
    }

    @GetMapping("/files")
    private List<String> getFiles(){
        return fileService.getFiles();
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadFile(FileRequest fileReq) {
        return fileService.upload(fileReq);
    }

    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> downloadFile(@RequestParam String pathName) throws IOException {
        return fileService.download(pathName);
    }
}
