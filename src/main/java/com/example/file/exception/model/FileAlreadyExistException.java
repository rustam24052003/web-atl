package com.example.file.exception.model;

public class FileAlreadyExistException extends RuntimeException{
    private String message;

    public FileAlreadyExistException() {}

    public FileAlreadyExistException(String message) {
        super(message);
        this.message = message;
    }
}
