package com.example.file.exception.model;

public class InvalidDirectoryOrNotExistException extends RuntimeException{
    private String message;

    public InvalidDirectoryOrNotExistException() {}

    public InvalidDirectoryOrNotExistException(String message) {
        super(message);
        this.message = message;
    }
}
