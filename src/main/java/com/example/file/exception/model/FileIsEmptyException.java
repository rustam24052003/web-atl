package com.example.file.exception.model;

public class FileIsEmptyException extends RuntimeException{
    private String message;

    public FileIsEmptyException() {}

    public FileIsEmptyException(String message) {
        super(message);
        this.message = message;
    }
}
